# This program runs a Discord bot that automatically spoilers messages
# Copyright (C) 2019 Emi Simpson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/agpl.txt>.

import discord
import yaml
from requests_futures.sessions import FuturesSession

import asyncio
import io
import re

cw_pattern = re.compile(r'CW(?:$|[\s:])\s*(.*)', flags=re.IGNORECASE)
filename_from_url_pattern = re.compile(r'/([^\n/?]+)/?(?:\?.*)?$')

class SpoilerBot(discord.Client):
	def __init__(self, server, channels, localization_info, cw_match = True, spoiler_by_reaction=False):
		super().__init__()
		self.server_id = server
		self.server = None
		self.channel_patterns = {re.compile(pattern.lower()) for pattern in channels}
		self.localization = localization_info
		self.ready = False
		self.cw_match = cw_match
		self.spoiler_by_reaction = spoiler_by_reaction
		self.requests_session = FuturesSession(max_workers=3)

	async def on_ready(self):
		self.server = self.get_guild(self.server_id)
		self.ready = True
		print(f'Logged in as {self.user}')
	
	async def on_message(self, message):
		if(self.ready and message.guild == self.server):
			in_mandatory_channel = any(pattern.fullmatch(message.channel.name) for pattern in self.channel_patterns)
			matched_cws = cw_pattern.search(message.content)
			if\
				in_mandatory_channel or\
				(self.cw_match and matched_cws)\
			:
				for attch in message.attachments:
					if attch.height and not attch.is_spoiler(): # An unspoilered image
						await self._on_message_cw(message, in_mandatory_channel, matched_cws)
						break
				for embed in message.embeds:
					urls = (url for url in [embed.thumbnail.url, embed.image.url, embed.video.url] if url != discord.Embed.Empty)
					if any(urls):
						await self._on_message_cw(message, in_mandatory_channel, matched_cws)

	async def on_message_edit(self, before, after):
		await self.on_message(after)

	async def _on_message_cw(self, message, in_mandatory_channel, matched_cws):
		# Build footer message
		footer = None
		if in_mandatory_channel:
			footer = self.localization['mandatory_channel']
		elif matched_cws:
			footer = self.localization['auto_cw_hint']
		footer += '  ' + self.localization['deletion_hint'] + '  ' + self.localization['license_notification']

		if matched_cws and matched_cws.lastindex: # Match found with 1+ group
			await self.repost(message, cw=matched_cws.group(1), footer=footer)
		else:
			await self.repost(message, footer=footer)

	async def repost(self, message, cw=None, footer=None):
		image_reqs = [
			(
				self.requests_session.get(attachment.proxy_url),
				attachment.filename
			)
			for attachment in message.attachments
		] + [
			(
				self.requests_session.get(embed.thumbnail.url),
				filename_from_url_pattern.search(embed.thumbnail.url).group(1)
			)
			for embed in message.embeds
			if embed.thumbnail.url != discord.Embed.Empty
		]

		if cw == '':
			cw = None

		# Create the embed
		embed = discord.Embed(
			type="rich",
			description=message.content,
			timestamp=message.created_at,
			color=message.author.color,
		)
		embed.set_author(
			name=f'{message.author.display_name} ({message.author})',
			icon_url=message.author.avatar_url,
		)
		if footer:
			embed.set_footer(
				text=footer
			)

		# Delete the old message
		delete_message_task = message.delete()

		# Create/Send the message
		send_message_task =  message.channel.send(
			content=f'**CW: {cw}**' if cw else None,
			embed=embed,
			files=[
				discord.File(
					fp=io.BytesIO(
						response.result().content
					),
					filename=filename,
					spoiler=True,
				)
				for response, filename in image_reqs
			],
		)

		# Once we get to this line, the attachments have all downloaded, since
		# that occurs when building the send_message_task

		# Concurrently delete the old message and send the new one
		asyncio.gather(delete_message_task, send_message_task)

	async def on_reaction_add(self, reaction, user):
		if reaction.message.guild != self.server:
			return

		if reaction.emoji == '🚫' and\
			reaction.message.author == self.user and\
			reaction.message.embeds and\
			any(str(user) in e.author.name for e in reaction.message.embeds):
				await reaction.message.delete()
		elif reaction.emoji == '🙈' and\
			self.spoiler_by_reaction and\
			(\
				any( # Image attachment in need of spoilering
					attch.height and not attch.is_spoiler()
					for attch in reaction.message.attachments
				) or\
				any( # Embeded url in need of spoilering
					any(
						url
						for url
						in [embed.thumbnail.url, embed.image.url, embed.video.url]
						if url != discord.Embed.Empty
					)
					for embed
					in reaction.message.embeds
				)\
			):
				await self.repost(
					reaction.message,
					footer = self.localization['reaction_spoilered'] + '  ' + self.localization['deletion_hint'] + '  ' + self.localization['license_notification']
				)

if __name__ == '__main__':
	config = None
	with open('config.yml') as config_file:
		config = yaml.load(config_file, Loader=yaml.Loader)
	bot = SpoilerBot(
		config['server'],
		config['channels'],
		config['localization'],
		config['cw_match'],
		config['spoiler_by_reaction'],
	)
	bot.run(config['token'])

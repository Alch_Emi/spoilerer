FROM python:3.10-alpine
VOLUME /config
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY main.py .

WORKDIR config
CMD python ../main.py
